// Import partials
// @codekit-prepend "partials/_global.js";
// @codekit-prepend "partials/_livesearch.js";
// @codekit-prepend "partials/_productcombinations.js";
// @codekit-prepend "partials/_updatecart.js";
// @codekit-prepend "partials/_form.js";
// @codekit-prepend "partials/_newphonenumber.js";
// @codekit-prepend "partials/_cookies.js";

var App = App || {};

$(document).ready(function() {

	App.LiveSearch.initialize();
	App.ProductCombinations.initialize();
	App.UpdateCart.initialize();
	App.Form.initialize();
	App.PhoneNumber.initialize();
	App.Cookies.initialize();

});
