var App = App || {};

/**
 * Update cart with ajax
 */
App.UpdateCart = (function() {

  var dom = {},

  // Current ajax request (for cancelling previous request before sending a new)
  currentRequest,

  cartRequest,

  // For live dom queries. Prevent elements from being cached and a reference to a selector
  selectors = {
    ajax_cart: ' .js-ajax-cart',
    post_form: '.js-form-post-on-document-ready',
  };

  function initialize() {

    // Store cached references to DOM elements
    // that will be used over and over again
    _setupDOM();

    // Add event listeners
    _addEventListeners();

    if( $( selectors.post_form ).length > 0 ) {
      _onFormChange();
    }
  }

  /**
   * Set up DOM (create?) elements and cache references for future use
   * @return {void}
   */
  function _setupDOM() {
    dom.$form_elements  = $( '.js-update-cart' );
    dom.$ajax_container = $( '.js-ajax-container' );

    dom.$form_post  = $( '.js-form-post' );

    // Use to add a checked class to a element
    dom.checked_element     = '.js-checked';
    // Product (15/15 Mbit)
    dom.$product            = $( '.js-product' );
  }


  /**
   * Attach event listeners to DOM elements
   */
  function _addEventListeners() {
    dom.$form_elements.on( 'change', _onFormChange );
  }

  /**
   * On Form Change post the form and run _loadCart()
   */
  function _onFormChange() {
    // Get the url
    var url = dom.$form_post.attr('action');
    // If there is a result;
    if (typeof (currentRequest) === 'object') {
      // abort request
      currentRequest.abort();
    }
    //ajax call here
    currentRequest = $.ajax({
      type: 'POST',
      url: url,
      data: dom.$form_post.serialize(),
      success: function () {
        _loadCart();
      }
    });
  }

  /**
   * Loads the cart with Ajax
   */
  function _loadCart() {
    var url = $( selectors.ajax_cart ).data('ajaxpath');
    $( selectors.ajax_cart ).addClass('loading');
    // If there is a result;
    if (typeof (cartRequest) === 'object') {
      // abort request
      cartRequest.abort();
    }
    //ajax call here
    cartRequest = $.ajax({
      url: url + '&t=' + new Date().getTime(),
      success: function ( data ) {
        dom.$ajax_container.html( $(data).find(selectors.ajax_cart) );
        $( selectors.ajax_cart ).removeClass('loading');
      }
    });

    // dom.$ajax_container.load( url + '&t=' + new Date().getTime() + selectors.ajax_cart, function() {
    //   $( selectors.ajax_cart ).removeClass('loading');
    // });
  }

  ////////////////
  // Public API //
  ////////////////

  return {
    initialize: initialize
  };

}());
