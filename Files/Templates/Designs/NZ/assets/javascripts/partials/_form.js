var App = App || {};

App.Form = (function() {

  var dom = {};

  function initialize() {
    // Set up DOM and cache references
    _setupDOM();

    // Add event listeners
    _addEventListeners();
  }

  /**
   * Set up DOM (create?) elements and cache references for future use
   * @return {void}
   */
  function _setupDOM() {
    dom.$form                   = $( 'form' );
    dom.$required               = $( '[required], .js-required', dom.$form );
    dom.$select_required        = $( 'select [required]', dom.$form );
    dom.$input_password         = $( '.js-password', dom.$form );
    dom.$input_password_confirm = $( '.js-password-confirm', dom.$form );
    dom.$form_fields            = $( '*', dom.$form );
    // Radio group required
    dom.$radio_element          = $( '.js-radio-group-required', dom.$form );

    // Form Post element
    dom.$form_post              = $( '.js-form-post' );

    // Add required
    dom.$tricker_button         = $( '.js-tricker-required' );
    dom.$target_element         = $( '.js-target-required' );
  }

  /**
   * Attach event listeners to DOM elements
   */
  function _addEventListeners() {
    dom.$form.on('submit', _onFormSubmit);
    dom.$form_fields.on('focusin', _onFormFieldsFocusIn);
    dom.$select_required.on('change', _onFormChange);
    dom.$form_fields.on('keyup', _onFormFieldsKeyUp);
    dom.$form_fields.on('blur', _onFormFieldsKeyUp);
    dom.$tricker_button.on( 'change', _onChangeTrickerButton );
    dom.$radio_element.on( 'change', _onRadioButtonChange );
  }

  function _onChangeTrickerButton() {
    if( dom.$tricker_button.prop('checked') ) {
      dom.$target_element.attr('required','true');
    }
    else {
      dom.$target_element.removeAttr('required');
    }
  }

  function _onRadioButtonChange() {
    dom.$radio_element.each(function() {
      $(this).removeClass('required');
    });
  }

  function _onFormFieldsFocusIn() {
    var $this = $(this);
    if( $this.hasClass('required') || $this.prop('required') === true ) {
      $this.removeClass('required');
    }
  }

  function _onFormChange() {
    var $this = $(this);
    if( dom.$form_fields.hasClass('required') || $this.prop('required') === true ) {
      _validation($this);
    }
  }

  function _onFormFieldsKeyUp() {
    var $this = $(this);
    if( $this.hasClass('required') || $this.prop('required') === true ) {
      _validation($this);
    }
  }

  function _validation($this, validation) {
    validation = false;
    var type = $this.attr('type');
    var length = $this.val().length;
    var value = $this.val();
    var mailVal = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    var numberVal = /[0-9]|\./;

    if( length < 2 ) {
      $this.removeClass('accepted');
      $this.addClass('required');
      validation = false;
    }
    else if( type === 'text' && length === 0 ) {
      $this.removeClass('accepted');
      $this.addClass('required');
      validation = false;
    }
    else if( type === 'number' && !numberVal.test($this.val()) ) {
      $this.removeClass('accepted');
      $this.addClass('required');
      validation = false;
    }
    else if( type === undefined && value === "0" ) {
      $this.removeClass('accepted');
      $this.addClass('required');
      validation = false;
    }
    else if( type === 'checkbox' && $this.prop('checked') === false ) {
      $this.removeClass('accepted');
      $this.addClass('required');
      validation = false;
    }
    else if( type === 'email' && !mailVal.test($this.val()) ) {
      $this.removeClass('accepted');
      $this.addClass('required');
      validation = false;
    }
    else if( type === undefined && value === "0" ) {
      $this.removeClass('accepted');
      $this.addClass('required');
      validation = false;
    }
    else if( $this.hasClass('js-password-confirm') === true && dom.$input_password.val() !== dom.$input_password_confirm.val() ) {
      $this.removeClass('accepted');
      $this.addClass('required');
      validation = false;
    }
    else {
      $this.removeClass('required');
      $this.addClass('accepted');
      validation = true;
    }
    return validation;
  }

  function _onFormSubmit(event) {
    var validation;
    var names = [];

    dom.$required.each(function() {
      var $this = $(this);
      if( $this.is(':visible') === true ) {
        validation = _validation($this, validation);
        if( validation === false ) {
          return false;
        }
      }
    });

    if( dom.$radio_element.is(':visible') === true ) {
      dom.$radio_element.each(function() {
        // Creates an array with the names of all the different checkbox group.
        names[$(this).attr('name')] = true;
      });

      // Goes through all the names and make sure there's at least one checked.
      for ( var name in names ) {
        var radio_buttons = $("input[name='" + name + "']");
        if (radio_buttons.filter(':checked').length === 0) {
          radio_buttons.removeClass('accepted');
          radio_buttons.addClass('required');
          return false;
        }
      }
    }

    if( validation === undefined && dom.$form_post.length > 0 || validation === true && dom.$form_post.length > 0 ) {
      _onFormPost( event, $(this) );
    }
    else {
      return validation;
    }
  }

  /**
   * Submits the form with ajax post
   */
  function _onFormPost( event, $form ) {
    $('.js-loader').addClass('loading');
    // prevent form submission
    event.preventDefault();
    // Get the url
    var url = $form.attr('action');
    //ajax call here
    $.ajax({
      type: 'POST',
      url: url,
      data: $form.serialize(),
      success: function (data) {
        // Data returns ex. /da-DK/Phone-Options.aspx
        if( $form.data('redirect') === undefined ) {
          if( data.length > 100 ) {
            window.location = "/Privat?nocustomerdata=true";
          }
          else {
            window.location = data;
          }
        }
        else {
          window.location = $form.data('redirect');
        }
        $('.js-loader').removeClass('loading');
      }
    });
  }


  ////////////////
  // Public API //
  ////////////////

  return {
    initialize: initialize
  };

})();
