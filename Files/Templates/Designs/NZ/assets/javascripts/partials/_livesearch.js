var App = App || {};

/**

LiveSearch is an ajax search that can be used in two ways:
- To return lists of links as known from regular live searches
- To return links with a custom click action. In this case the custom click action is defined in _onResultItemClick, and makes an ajax request on item click, based on a webservice defined in a hidden field in the form.

Markup required for regular live search:

<form class="js-livesearch js-livesearch-loader" action="/CustomHandlers/LookupAddress.ashx">
  <input type="hidden" class="js-noresults" value="No results found">
  <input type="hidden" class="js-error" value="An error has occurred">
  <input type="hidden" class="js-result-item-class" value="search-item">
  <input type="text" class="js-input" value="" size="100">
  <input type="submit" class="js-submit-button" value="Submit">
  <ul class="results js-results"></ul>
</form>

Markup required from live search with custom click action on results:

<form class="js-livesearch js-livesearch-loader" action="/CustomHandlers/LookupAddress.ashx">
  <input type="hidden" class="js-itemclick-action" value="/CustomHandlers/LookupAddress.ashx">
  <input type="hidden" class="js-noresults" value="No results found">
  <input type="hidden" class="js-error" value="An error has occurred">
  <input type="hidden" class="js-result-item-class" value="search-item">
  <input type="text" class="js-input" value="" size="100">
  <input type="submit" class="js-submit-button" value="Submit">
  <ul class="results js-results"></ul>
</form>

*/
App.LiveSearch = (function() {
  'use strict';

      // Selectors for DOM elements
  var selectors = {
        form              : '.js-livesearch',
        loader            : '.js-livesearch-loader',
        input             : '.js-input',
        results           : '.js-results',
        noResults         : '.js-noresults',
        resultItemClass   : '.js-result-item-class',
        error             : '.js-error',
        clickAction       : '.js-itemclick-action',
        lookUpUserAddress : '.js-look-up-user-address'
      },

      loadAjaxEvent = $.Event( 'AjaxSuccess' ),

      // Class names to toggle with js
      classNames = {
        loading : 'loading',
        message : 'msg',
        active  : 'active'
      },

      // Store cached references to DOM elements
      // that will be used over and over again
      dom = {},

      // Minimum characters in search input before firing ajax call
      minInputLength = 4,

      // Webservice for live search
      ajaxUrl,

      // Webservice for item click posts
      ajaxUrlClick,

      // Timeout on keyup before firing ajax call
      keyupTimeout = 350, // ms

      // Current timeout on keyup (for clearing previous keyup timeout)
      currentTimeout,

      // Current ajax request (for cancelling previous request before sending a new)
      currentRequest,

      // Feedback message to user when no results
      noResultsMsg,

      // The result list item class
      ResultItemClass,

      // Feedback to user on ajax error
      errorMsg,

      // Checks if ajax request has results
      hasResults = false;

  /**
   * Initialize function
   * @return {void}
   */
  function initialize() {
    // Set up DOM and cache references
    _setupDOM();

    // Add event listeners
    _addEventListeners();

    // Feedback messages
    noResultsMsg      = $( selectors.noResults, dom.$form ).val();
    errorMsg          = $( selectors.error, dom.$form ).val();

    // Result item class
    ResultItemClass   = $( selectors.resultItemClass, dom.$form ).val();

    // Webservice URL's
    ajaxUrlClick      = $( selectors.clickAction, dom.$form ).val();
    ajaxUrl           = dom.$form.attr( 'action' );
  }

  /**
   * Set up DOM (create?) elements and cache references for future use
   * @return {void}
   */
  function _setupDOM() {
    dom.$form                       = $( selectors.form );

    // Search input
    dom.$input                      = $( selectors.input, dom.$form );

    // Loader class
    dom.$loader                     = $( selectors.loader );

    // Target element
    dom.$results                    = $( selectors.results, dom.$form );

    dom.$close_box                  = $( '.js-close-box' );
    dom.$fiber_box_with_result      = $( '.js-fiber-found' );
    dom.$fiber_box_without_result   = $( '.js-fiber-not-found' );
    dom.$fiber_box_no_address       = $( '.js-address-not-found' );
    dom.$fiber_box_user_found       = $( '.js-user-found' );
    dom.$submit_button              = $( '.js-submit-button' );

  }

  /**
   * Attach event listeners to DOM elements
   */
  function _addEventListeners() {
    dom.$form.on( 'submit', _onFormSubmit );
    dom.$close_box.on( 'click', _onCloseClick );

    // On key down, select the first result element
    dom.$input.on( 'keyup', _onInputKeyup );

    // Trigger a ajax function when typing
    dom.$input.on('keydown', _onInputKeydown );
    dom.$form.on( 'click', selectors.lookUpUserAddress, _onUserAddressClick );

    dom.$input.on( 'blur', _onInputBlur );
  }

  function _onInputBlur() {
    if( $(selectors.results).length === 0 ) {
      $('.js-address-container').val( $(this).val() );
      $(selectors.results).empty();
    }
  }

  function _onUserAddressClick() {
    var $this = $(this);
    var data = $this.find('a').attr('data-src');
    var html = $this.find('a').html();

    dom.$input.val(html);
    $('.js-address-container').val(data);
    $(selectors.results).empty();
  }


  /**
   * _onCloseClick - Removes active class which hides the pop-up box
   */
  function _onCloseClick() {
    dom.$fiber_box_with_result.removeClass( classNames.active );
    dom.$fiber_box_without_result.removeClass( classNames.active );
    dom.$fiber_box_no_address.removeClass( classNames.active );
    dom.$fiber_box_user_found.removeClass( classNames.active );
  }

  /**
   * [_onResultsKeydown]
   * Enables arrow up and down to navigate the resultlist
   */
  function _onResultsKeydown(e) {
    var $this = $(this);
    var $nextitem = $this.next('li');
    var $previtem = $this.prev('li');

    // Focus Down
    if (e.keyCode === 40 && typeof $nextitem !== 'undefined') {
      $nextitem.focus();
    }
    // Focus Up
    else if (e.keyCode === 38 && typeof $previtem !== 'undefined') {
      $previtem.focus();
    }
    if( e.keyCode === 32 || e.keyCode === 13 ) { /* if enter or space */
      $this.find('a').trigger('click');
    }
  }

  /**
   * On input keydown find the first result element and focus
   */
  function _onInputKeydown(e) {
    // Focus Down
    if (e.keyCode === 40) {
      $(this).blur();
      dom.$results.find('li:first').focus();
    }
  }


  /**
   * Event handler for 'keyup' on input
   * Trigger a _getAjax function on Keyup
   */
  function _onInputKeyup( event ) {
    event.preventDefault();
    var $this = $(this);
    var query = $this.val();
    // Only run _getAjax key up is different from enter, arrow up, down, left or right.
    if ( query.length >= minInputLength && event.which !== 13 && event.which !== 38 && event.which !== 40 && event.which !== 37 && event.which !== 39 ) {
      $this.closest(dom.$loader).addClass( classNames.loading );
      // clear timeout
      clearTimeout(currentTimeout);
      // set timeout
      currentTimeout = setTimeout(function () {
        // on timeout
        var $form  = $this.closest(dom.$form);
        _getAjax( query, $form );
      }, keyupTimeout);
    }
  }

  /**
   * Event handler for 'submit' on form
   * Ajax request if only possible when there are no visible results
   */
  function _onFormSubmit( event ) {
    event.preventDefault();
    var data = dom.$input.val();
    if ( data.length >= minInputLength && !hasResults ) {
      _onPostAjax( { 'queryText' : data } );
    }

  }

  /**
   * Call to webservice for live results
   */
  function _getAjax( query, $form ) {
    // If there is a result;
    if (typeof (currentRequest) === 'object') {
      // abort request
      currentRequest.abort();
    }
    // send request
    if( typeof( ajaxUrl ) !== 'undefined' ) {
      currentRequest = $.get( ajaxUrl ,
      { queryText: query },
      function ( data ) {
        // if data available
        if ( data.length === 0 ) {
          var $msg = $('<li/>').addClass( classNames.message ).text( noResultsMsg );
          $form.find( dom.$results ).html( $msg );
          $form.find( dom.$submit_button ).removeClass('hide');
          hasResults = false;
        }
        else {
          // Empty results
          $form.find( dom.$results ).html('');
          $form.find( dom.$submit_button ).addClass('hide');
          $.each(data, function (i, result) {
            var $link = $('<a/>').text( result.label );
            var $li = $('<li class="'+ResultItemClass+'" tabindex='+i+'/>').append($link);

            // Append click function if ajax url is defined
            if( typeof( ajaxUrlClick ) !== 'undefined' ) {
              $link.attr( 'data-src', result.value );
              $link.on('click', _onResultItemClick );
            }
            else {
              $link.attr( 'href', result.value );
            }
            $form.find( dom.$results ).append($li);
          });
          hasResults = true;
          $form.find( dom.$results ).find('li').on('keydown', _onResultsKeydown );
        }
      })
      .error(function () {
        var $msg = $('<li/>').addClass( classNames.message ).text( errorMsg );
        $form.find( dom.$results ).html($msg);
      })
      .always(function () {
        $form.find( dom.$loader ).removeClass( classNames.loading );
      });
    }
  }

  /**
   * Event handler for 'click' on result items
   */
  function _onResultItemClick( event ) {
    event.preventDefault();
    var $this = $( event.currentTarget );
    var data = $this.attr('data-src');

    $this.addClass( classNames.loading );
    _onPostAjax( event, { 'returnVal' : data } );
  }

  /**
   * _onPostAjax - Ajax post data
   * If response/data available show pop-up (fiber_box_with_result)
   * If response/data is not available show pop-up (fiber_box_without_result)
   */
  function _onPostAjax( event, data ) {
    $.post( ajaxUrlClick,
      data,
      function(response) {
        if( response !== '-1' && response !== '-2' && response.length > 0 ) {
          dom.$fiber_box_with_result.addClass( classNames.active );
          dom.$fiber_box_with_result.trigger( loadAjaxEvent );
        }
        else if( response === '-2' && response.length > 0 ) {
          dom.$fiber_box_user_found.addClass( classNames.active );
          dom.$fiber_box_user_found.trigger( loadAjaxEvent );
        }
        else if( response.length === 0 ) {
          dom.$fiber_box_no_address.addClass( classNames.active );
          dom.$fiber_box_no_address.trigger( loadAjaxEvent );
        }
        else {
          dom.$fiber_box_without_result.addClass( classNames.active );
          dom.$fiber_box_without_result.trigger( loadAjaxEvent );
        }
        if( typeof(event) !== 'undefined' ) {
          $( event.currentTarget ).removeClass( classNames.loading );
        }
      });
  }

  ////////////////
  // Public API //
  ////////////////

  return {
    initialize: initialize
  };

})();
