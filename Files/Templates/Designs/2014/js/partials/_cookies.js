var App = App || {};

/**
 * Querystring redirect function depended on the checkout flow state.
 */
App.Cookies = (function() {

  var dom = {};

  function initialize() {
    // Add event listeners
    _querystringRedirect();
  }

  /**
   * If a user leaves the checkout flow before completion eg. http://www.bredbaandnord.dk/Bestil/Telfonvalg
   * The next time the user clicks on the "Bestil" button the user will be redirect to the last known state in the checkout flow.
   */
  function _querystringRedirect() {
    if( window.location.pathname.indexOf('Bestil') > -1 ) {
      localStorage.setItem( 'qState', window.location.pathname );
    }
    else {
      if( localStorage.getItem('qState') !== null ) {
        $("#mainmenu a[href='/Bestil']").attr('href', localStorage.getItem('qState') );
      }
    }
  }



  ////////////////
  // Public API //
  ////////////////

  return {
    initialize: initialize
  };

}());
