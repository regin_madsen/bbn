var App = App || {};

App.Form = (function() {

  var dom = {},
  phoneAjaxTimeout = 350,
  phonevalidation = 0;
  function initialize() {
    // Set up DOM and cache references
    _setupDOM();

    // Add event listeners
    _addEventListeners();
  }

  /**
   * Set up DOM (create?) elements and cache references for future use
   * @return {void}
   */
  function _setupDOM() {
    //dom.$form                   = $( 'form:not(.abide)' );
    dom.$form                   = $( 'form' );
    //dom.$formabide                   = $( 'form.abide' );
    dom.$required               = $( '[required], input.js-required', dom.$form );
    dom.$phone_valid            = $('.js-phone');

    dom.$select_required        = $( 'select.js-required', dom.$form );
    dom.$input_password         = $( '.js-password', dom.$form );
    dom.$input_toupper         = $( '.js-to-upper');
    dom.$input_password_confirm = $( '.js-password-confirm', dom.$form );
    dom.$form_fields            = $( '*', dom.$form );
    // Radio group required
    dom.$radio_element          = $( '.js-radio-group-required', dom.$form );
    // Form Post element
    dom.$form_post              = $( '.js-form-post' );

    // Add required
    dom.$tricker_button         = $( '.js-tricker-required' );

    dom.$target_element         = $( '.js-target-required' );
    dom.$target_phone_element            = $('.js-target-phone');
  }

  /**
   * Attach event listeners to DOM elements
   */
  function _addEventListeners() {
     //dom.$formabide.on('valid invalid submit', _onFormSubmitAbide);
     //dom.$formabide.on('valid', _onFormSubmitAbideValid);
     if(dom.$form.hasClass('abide')){

      dom.$form.on('valid invalid submit', _onFormSubmitAbide);
      dom.$form.on('valid', _onFormSubmitAbideValid);
     }else{

      dom.$form.on('submit', _onFormSubmit);
     }
     dom.$form_fields.on('focusin', _onFormFieldsFocusIn);
     dom.$input_toupper.on('focusout', _onToUpBlur);
     dom.$select_required.on('change', _onFormChange);
     dom.$form_fields.on('keyup', _onFormFieldsKeyUp);
     dom.$form_fields.on('blur', _onFormFieldsKeyUp);
     dom.$tricker_button.on( 'change', _onChangeTrickerButton );
     dom.$radio_element.on( 'change', _onRadioButtonChange );

  }

  function _onChangeTrickerButton() {
    if(dom.$target_element.hasClass("js-target-required")){
    if( dom.$tricker_button.prop('checked') ) {

      dom.$target_element.removeAttr('required');
      if(dom.$target_element.hasClass('js-target-phone')){
        dom.$target_phone_element.removeClass('js-phone');
        dom.$phone_valid = $('.js-phone');
      }
    }
    else {
      dom.$target_element.attr('required','true');
      if(dom.$target_element.hasClass('js-target-phone')){
        dom.$target_phone_element.addClass('js-phone');
        dom.$phone_valid = $('.js-phone');
      }

    }
    }
  }

  function _onRadioButtonChange() {
    dom.$radio_element.each(function() {
      $(this).removeClass('required');
    });
  }

  function _onFormFieldsFocusIn() {

    var $this = $(this);
    if( $this.hasClass('required') || $this.prop('required') === true ) {
      $this.removeClass('required');
    }
  }
  function _onFormFieldsFocusOut() {

    var $this = $(this);

    if( $this.hasClass('required') || $this.prop('required') === true ) {
      $this.removeClass('required');
    }
  }

  function _onFormChange() {
    var $this = $(this);
    if( dom.$form_fields.hasClass('required') || $this.prop('required') === true ) {
      _validation($this);
    }
  }
function _onToUpBlur() {

    var $this = $(this),
         text = $this.val(),
        split = text.split(" "),
          res = [],
          i,
          len,
          component;

        if(text.length>0){
        for (i = 0, len = split.length; i < len; i++) {
            component = split[i];
            res.push(component.substring(0, 1).toUpperCase());
            res.push(component.substring(1));
            res.push(" "); // put space back in
        }
        $this.val(res.join(""));
        }


  }

  function _onFormFieldsKeyUp() {

    var $this = $(this);
    if( $this.hasClass('required') || $this.prop('required') === true ) {
      _validation($this);
    }

  }

  function _validation($this, validation) {

    validation = false;
    var type = $this.attr('type');
    var length = $this.val().length;
    var value = $this.val();
    var mailVal = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    var numberVal = /[0-9]|\./;

    if( length < 2 ) {
      $this.removeClass('accepted');
      $this.addClass('required');
      validation = false;
    }
    else if( type === 'text' && length === 0 ) {
      $this.removeClass('accepted');
      $this.addClass('required');
      validation = false;
    }
    else if( type === 'number' && !numberVal.test($this.val()) ) {
      $this.removeClass('accepted');
      $this.addClass('required');
      validation = false;
    }
    else if( type === undefined && value === "0" ) {
      $this.removeClass('accepted');
      $this.addClass('required');
      validation = false;
    }
    else if( type === 'checkbox' && $this.prop('checked') === false ) {
      $this.removeClass('accepted');
      $this.addClass('required');
      validation = false;
    }
    else if( type === 'email' && !mailVal.test($this.val()) ) {
      $this.removeClass('accepted');
      $this.addClass('required');
      validation = false;
    }
    else if( type === undefined && value === "0" ) {
      $this.removeClass('accepted');
      $this.addClass('required');
      validation = false;
    }
    else if( $this.hasClass('js-password-confirm') === true && dom.$input_password.val() !== dom.$input_password_confirm.val() ) {
      $this.removeClass('accepted');
      $this.addClass('required');
      validation = false;
    }
    else {
      $this.removeClass('required');
      $this.addClass('accepted');
      validation = true;


    }



    return validation;
  }
function _onFormSubmitAbide(event) {

    event.preventDefault();
if(dom.$phone_valid.length > 0){
      phonevalidation = dom.$phone_valid.length; //nulstil validering. Der findes muligvis en mere effektiv metode, da vi gør sådan for ikke at låse ved hvert ajax request
     dom.$phone_valid.each(function(){
          var $this = $(this);
          if( $this.is(':visible') === true) {
           _validPhone($this);


          }
        });
   }

    return false;
  }
  function _onFormSubmitAbideValid(event) {


        event.preventDefault();

    if(dom.$phone_valid.length > 0){
      phonevalidation = dom.$phone_valid.length; //nulstil validering. Der findes muligvis en mere effektiv metode, da vi gør sådan for ikke at låse ved hvert ajax request
     dom.$phone_valid.each(function(){
          var $this = $(this);
          if( $this.is(':visible') === true) {
           _validPhone($this);


          }
        });
    clearTimeout(phoneAjaxTimeout);

    phoneAjaxTimeout = setTimeout(function () {

      if(phonevalidation === 0) { //Hvis phonevalidation er større end 0 er der fejl. Validering nulstilles ved hvert "submit"

              _onFormSubmit(event);

        }
        else {

          return false;
        }

    }, phoneAjaxTimeout);
  }

}
  function _onFormSubmit(event) {


    var validation;
    var names = [];



    dom.$required.each(function() {
      var $this = $(this);
      if( $this.is(':visible') === true ) {
        validation = _validation($this, validation);

        if( validation === false ) {
          return false;
        }
      }
    });



    if( dom.$radio_element.is(':visible') === true ) {
      dom.$radio_element.each(function() {
        // Creates an array with the names of all the different checkbox group.
        names[$(this).attr('name')] = true;
      });

      // Goes through all the names and make sure there's at least one checked.
      for ( var name in names ) {
        var radio_buttons = $("input[name='" + name + "']");
        if (radio_buttons.filter(':checked').length === 0) {
          radio_buttons.removeClass('accepted');
          radio_buttons.addClass('required');
          return false;
        }
      }
    }




    if( validation === undefined && dom.$form_post.length > 0 || validation === true && dom.$form_post.length > 0 ) {

      _onFormPost( event, $(this) );
    }
    else {

      return validation;
    }
  }
/*
_validPhone virker PT KUN med abide forms. domSetup skal laves om så det er samme dom object der benyttes
uanset om det er abide eller ej, og så skal der laves check af hvorvidt det er en abide form istedet i eks.
_omFormSubmit
*/
function _validPhone($this){

      var phoneNumber = $this.val();

      $.ajax({
      type: 'POST',
      async: false,
      url: "/CustomHandlers/TjeckNummer.ashx?number="+phoneNumber,
      data: dom.$form.serialize(),
      success: function (data) {
        if(data.Result===false){
            $this.parent("div").addClass('error');
            $this.next("small").text(data.Cause);

        }else{
          $this.parents("div").removeClass('error');
          phonevalidation--;
        }


        // Data returns ex. /da-DK/Phone-Options.aspx
        }
    });
}
  /**
   * Submits the form with ajax post
   */
  function _onFormPost( event, $form ) {

    if($form.attr('action') === undefined){
      $form = $("#form.abide");
    }


    $('.js-loader').addClass('loading');
    // prevent form submission
    event.preventDefault();
    // Get the url
    var url = $form.attr('action');
    //ajax call here

    $.ajax({
      type: 'POST',
      url: url,
      data: $form.serialize(),
      success: function (data) {

        // Data returns ex. /da-DK/Phone-Options.aspx
        if($form.hasClass('js-show-modal')){
          dom.$successModal = $("."+$form.attr("data-modal"));
          if(data.Sent===false){
            dom.$successModal.find("h3").text('Beskeden blev ikke sendt');
            dom.$successModal.find("p").text(data.Message);

          }
          $("."+$form.attr("data-modal")).foundation('reveal', 'open');
        }

        else if( $form.data('redirect') === undefined ) {
          if( data.length > 100 ) {
            window.location = "/Privat?nocustomerdata=true";
            //return false;
          }
          else {
            window.location = data;

          }
        }
        else {
          window.location = $form.data('redirect');

        }
        $('.js-loader').removeClass('loading');
      }
    });
 }


  ////////////////
  // Public API //
  ////////////////

  return {
    initialize: initialize
  };

})();