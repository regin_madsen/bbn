var App = App || {};

/**
 * PhoneNumber is used when a user wants to search after a new phone number.
 */
App.PhoneNumber = (function() {

  var dom = {},

  timer,

  // For live dom queries. Prevent elements from being cached and a reference to a selector
  selectors = {
    load_page: '.js-load-result',
    available_number: '.js-available-phone-number',
  };

  function initialize() {
    // Set up DOM and cache references
    _setupDOM();

    // Add event listeners
    _addEventListeners();
  }

  /**
   * Set up DOM (create?) elements and cache references for future use
   * @return {void}
   */
  function _setupDOM() {
    dom.$body               = $('body');
    dom.$search_element     = $( '.js-phone-number-search' );
    dom.$search_button      = $( '.js-phone-number-button' );
    dom.$result_container   = $( '.js-phone-number-results' );
    dom.$new_phone_number   = $( '.js-new-phone-number' );
  }

  /**
   * Attach event listeners to DOM elements
   */
  function _addEventListeners() {
    dom.$search_element.on( 'keyup', _onSearchElementKeyUp );
    dom.$search_button.on( 'click', _loadResults );
    dom.$body.on( 'click', selectors.load_page, _onLoadPageClick );
    dom.$body.on( 'click', selectors.available_number, _onAvailableNumberClick );
  }

  /**
   * On Search element key up, run getAjax function width a small delay
   */
  function _onSearchElementKeyUp() {
    if(timer) {
      clearTimeout(timer);
    }
    timer = setTimeout(function () {
      _loadResults();
    }, 300);
  }

  /**
   * Pick the value (phone number) and append it to a element
   */
  function _onAvailableNumberClick() {
    var v = $(this).val();
    dom.$new_phone_number.val(v);
  }

  /**
   * When clicking on the buttons, return the pagenumber and do the ajax dance.
   */
  function _onLoadPageClick() {
    var pagenumber = $(this).data('page');
    _loadResults( pagenumber );
  }

  /**
   * Load results and append it to the results container element
   */
  function _loadResults( pagenumber ) {
    // The path to the result page
    var url = dom.$search_element.data('url');
    // The number of results
    var count = dom.$search_element.data('result-count');
    // The loading class
    var loadClass = dom.$search_element.data('loading-class');
    // The search value
    var val = dom.$search_element.val();
    // Makes sure pagenumber exists and is not an object
    var page = (typeof( pagenumber ) !== 'object')? pagenumber : 0;

    // Add the loading class
    dom.$result_container.addClass(loadClass);
    // Do the jquery ajax dance
    dom.$result_container.load( url + '?count=' + count + '&q=' + val + '&page=' + page + ' .js-new-phone-data', function() {
      dom.$result_container.removeClass(loadClass);
    });
  }





  ////////////////
  // Public API //
  ////////////////

  return {
    initialize: initialize
  };

}());
