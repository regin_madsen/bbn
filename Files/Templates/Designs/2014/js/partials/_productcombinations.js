var App = App || {};

/**
 * http://bbn.staging.nozebrahosting.dk/da-DK/Product-list.aspx
 * ProductCombinations controles the product and productgroups (packages)
 */
App.ProductCombinations = (function() {

  // Store cached references to DOM elements
  // that will be used over and over again
  var dom = {};

  function initialize() {
    // Set up DOM and cache references
    _setupDOM();

    // Add event listeners
    _addEventListeners();

    if( dom.$first_package.length > 0 ) {
      dom.$first_package.trigger('click');
    }

    // If no groups are available run _setProductsToChecked function
    if( dom.$group.length === 0 ) {
      _setProductsToChecked();
    }

    else {
      // Validate form
      _formValidate();
    }
  }

  /**
   * Set up DOM (create?) elements and cache references for future use
   * @return {void}
   */
  function _setupDOM() {
    // Product group (Waoo! 1)
    dom.$group              = $( '.js-group' );
    // Product (15/15 Mbit)
    dom.$product            = $( '.js-product' );
    // Additional product (Telefonsvarer)
    dom.$additional_product = $( '.js-additional-product' );
    // Additional group (All aditional products)
    dom.$additional_group   = $( '.js-additional-group' );
    // Use to add a checked class to a element
    dom.checked_element     = '.js-checked';

    dom.$first_package      = $( '.js-first-package-selected' );
  }

  /**
   * Attach event listeners to DOM elements
   */
  function _addEventListeners() {
    dom.$group.on( 'change', _onGroupChange );
    dom.$product.on( 'change', _onProductChange );
    dom.$additional_product.on( 'change', _onAdditionalProductChange );
    dom.$additional_group.on( 'change', _onAdditionalGroupChange );
  }


  function _addCheckedClass() {
    $('.checked').removeClass('checked');
    $('input:checked').each(function() {
      $(this).closest(dom.checked_element).addClass('checked');
      $(this).siblings(dom.checked_element).addClass('checked');
    });
  }

  /**
   * [_onGroupChange]
   * Select inputs from data-combination
   */
  function _onGroupChange() {
    var combination = $(this).data('combination').toString().split('_');
    dom.$product.prop("checked", false);
    for (var i = 0; i < combination.length; i++) {
      $('input[value='+combination[i]+']').prop('checked', true);
    }
    var internetProductIsChecked = $('input[name="internet-product"]:checked').length > 0;
    var phoneProductIsChecked = $('input[name="phone-product"]:checked').length > 0;
    var tvProductIsChecked = $('input[name="tv-product"]:checked').length > 0;

    if( internetProductIsChecked !== true ) {
      $('input[name="internet-product"][value="-1337"]').prop('checked', true);
    }
    if( phoneProductIsChecked !== true ) {
      $('input[name="phone-product"][value="-1337"]').prop('checked', true);
    }
    if( tvProductIsChecked !== true ) {
      $('input[name="tv-product"][value="-1337"]').prop('checked', true);
    }

    _addCheckedClass();

  }

  /**
   * _setFirstProductToChecked
   */
  function _setProductsToChecked() {
    // Add the first internet produt to the cart
    $('input[name="internet-product"]:first()').trigger('click');
    // Add the last phone produt to the cart
    $('input[name="phone-product"]:last()').trigger('click');
    // Add the last tv produt to the cart
    $('input[name="tv-product"]:last()').trigger('click');
  }

  function _formValidate() {
    if( dom.$product.length > 0 ) {
      var productType = '';
      var validate = 0;
      var checked = 0;
      dom.$product.each(function() {
        var $this = $(this);
        if( productType !== $this.attr('name') ){
          validate++;
        }
        if( $this.prop('checked') ) {
          checked++;
        }
        productType = $this.attr('name');
      });
      if( validate !== checked || $('input:checked[value="-1337"]').length === validate ) {
        $('.js-form-post').find(':submit').attr('disabled', true);
      }
      else {
        $('.js-form-post').find(':submit').attr('disabled', false);
      }
    }
  }

  /**
   * [_onProductChange]
   * Loop product values and if the values match group data-combination, select the group.
   */
  function _onProductChange() {
    var combination = "";
    dom.$product.each(function() {
      var $this = $(this);
      var val = parseInt( $this.val() );
      if( $this.prop("checked") && val !== -1337 ) {
        combination = combination + '_' + $this.val();
      }
      // If No internet is selected, disable Viasat TV products
      if( $('input[name="internet-product"][value="-1337"]').prop('checked') ) {
        if( $('input[name="phone-product"]').val() !== "-1337" ) {
          $('input[name="phone-product"]').attr('disabled', true).prop('checked',false);
          $('input[name="phone-product"][value="-1337"]').prop('checked', true);
        }
        if( val < 0 && val > -1337 ) {
          $this.addClass('disabled').attr('disabled',true);
          if( $this.prop('checked') ) {
            $('input[name="tv-product"][value="-1337"]').prop('checked', true);
          }
        }
      }
      else {
        $('input[name="phone-product"]').attr('disabled', false);
        $this.removeClass('disabled').attr('disabled',false);
      }
    });
    dom.$group.prop('checked', false);
    $('input[data-combination="'+combination.substring(1)+'"]').prop("checked", true);
    _addCheckedClass();
    _formValidate();
  }

  /**
   * [_onAdditionalProductChange]
   * Loop additional products and if all additional products is checked then set the additional group product to checked.
   */
  function _onAdditionalProductChange() {
    var checked = false;
    dom.$additional_product.each(function() {
      if( $(this).prop("checked") ) {
        checked = true;
      }
      else {
        checked = false;
        return false;
      }
    });
    if( checked === true ) {
      dom.$additional_group.prop('checked', true);
    }
    else {
      dom.$additional_group.prop('checked', false);
    }
    _addCheckedClass();
  }

  /**
   * [_onAdditionalGroupChange]
   * If additional group is checked, set alle additional products to checked
   */
  function _onAdditionalGroupChange() {
    var $this = $(this);
    if( $this.prop('checked') ) {
      dom.$additional_product.prop('checked', true);
    }
    else {
      dom.$additional_product.prop('checked', false);
    }
    _addCheckedClass();
  }


  ////////////////
  // Public API //
  ////////////////

  return {
    initialize: initialize
  };

}());
