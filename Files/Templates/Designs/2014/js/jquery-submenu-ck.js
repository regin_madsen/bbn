(function(e)
	{e.fn.spasticNav=function(t)
		{t=e.extend({overlap:20,speed:500,reset:1500,color:"#0b2b61",easing:"easeOutExpo"},t)
		;return this.each(function(){var n=e(this),r=e("#selected",n),i,s;e('<li id="blob"></li>')
		.css({width:r.outerWidth(),height:r.outerHeight()+t.overlap,left:r.position()
			.left,top:r.position().top-t.overlap/2,backgroundColor:t.color})
		.appendTo(this);i=e("#blob",n);e("li:not(#blob)",n).hover(function(){clearTimeout(s);i
	   .animate({left:e(this).position().left,width:e(this).width()},
	   	{duration:t.speed,easing:t.easing,queue:!1})},function()
	   {s=setTimeout(function(){i.animate({width:r.outerWidth(),:r.position().left},t.speed)},t.reset)})})}})(jQuery);