  $(function(){
    $(document).foundation('reveal topbar forms dropdown').foundation('section',{multi_expand:true,one_up:true,deep_linking:true}).foundation('abide',{patterns: {
      intonly: /^\d+$/
    },live_validate : false});
    $("img.lazy").lazyload({ effect : "fadeIn"});
    $(".section-container.button-group section:first-child").addClass("active");
  });


  $(".myBox:not(.nolink), .myBox2").has("a").addClass('boxeffect');
  $(".myBox[data-reveal-id]").addClass('boxeffect');

$('.close-reveal-button').click(function(){
    $(this).parents(".reveal-modal").foundation('reveal', 'close');
});

  $(".boxeffect:not(.nolink)").click(function(e){
    if($(this).attr("data-reveal-id")){
        e.preventDefault();
        $('#'+$(this).attr("data-reveal-id")).foundation('reveal', 'open');

    }
        else{
        if($(this).find("a").attr("target") === "_blank"){
            window.open($(this).find("a").attr("href"),"_blank");
        }else{
            window.open($(this).find("a").attr("href"),"_top");
        }
        }
            return false;
  });
    $('.active').on('shown.active', function(){
        $(this).parent().find(".fi-plus").removeClass("fi-plus").addClass("fi-minus");
        }).on('hidden.bs.active', function(){
        $(this).parent().find(".fi-minus").removeClass("fi-minus").addClass("fi-plus");
    });
        $('#search-trigger, #search .close')
        .bind('click', function (e) {
            var search = $('#search');
            $('nav > ul > li > a')
                .trigger('collapse');
            $('.nav-toggle')
                .trigger('collapse');
            if (search.is(':hidden')) {
                $(".has-dropdown.inpath .dropdown").hide();
                $("#sidebar, .mainIndhold").addClass("search-slided");
                search.slideDown(1, function () {
                    $('.search-query')
                        .focus();
                });

            } else {
                search.slideUp(1);
                $("#sidebar, .mainIndhold").removeClass("search-slided");
                $(".has-dropdown.inpath .dropdown").show();

            }
            e.preventDefault();
        });
    $('#search')
        .keydown(function (e) {
            if (e.keyCode === 27) {
                $('#search .close')
                    .trigger('click');
            }
        });
    $('#search .close')
        .bind('click', function (e) {
            $('.search-query')
                .val('');
            $('#search')
                .slideUp(1);


            $('#search-trigger')
                .focus();
            e.preventDefault();
        });
    if ($('body')
        .hasClass('search-results')) {
        $('#search')
            .slideDown();

    }
//     $(document).on("click",function (e) {
//     e.stopPropagation();
//     var container = $(".has-dropdown");
// })


    $(document).on("click touchstart", function (e)
{
    var container = $(".js-livesearch-loader");

    if (!container.is(e.target) && container.has(e.target).length === 0) // ... nor a descendant of the container
    {
        $(".js-results").html("");
        $(".js-input").blur();
    }
});
$("input[type=submit]").on("click",function(event){

    if($('input[type=checkbox].js-radio-group-required').is(":visible")==true && $('input[type=checkbox].js-radio-group-required:checked').length==0){
     $("input[type=checkbox].js-radio-group-required").parents("div.js-groupwrapper").find("small.error").show();
        return false;
    }else{
        $("input[type=checkbox].js-radio-group-required").parents("div.js-groupwrapper").find("small.error").hide();
        return true;
    }

});

  $('#mainmenu').spasticNav();