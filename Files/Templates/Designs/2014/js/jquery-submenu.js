(function($,sr){

  // debouncing function from John Hann
  // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
  var debounce = function (func, threshold, execAsap) {
      var timeout;

      return function debounced () {
          var obj = this, args = arguments;
          function delayed () {

              if (!execAsap)
                  func.apply(obj, args);
              timeout = null;
          };

          if (timeout)
              clearTimeout(timeout);
          else if (execAsap)
              func.apply(obj, args);

          timeout = setTimeout(delayed, threshold || 100);
      };
  }
  // smartresize

  jQuery.fn[sr] = function(fn){  return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr); };

})(jQuery,'smartresize');

(function($) {

	$.fn.spasticNav = function(options) {

		options = $.extend({
			overlap : 20,
			speed : 600,
			reset : 1500,
			color : '#d7cfbf',
			easing : 'easeInOutQuint'
		}, options);

		return this.each(function() {

			var nav = $(this),
			currentPageItem = $('li.inpath', nav),
			blob,
			pos,
			reset;
			if(currentPageItem.length===0){
				pos=-100;
				currentPageItem = $('li:first-child', nav);
			}else{pos = currentPageItem.position().left + (currentPageItem.innerWidth()/2)-8;}

			$('<li id="blob"></li>').css({
				left : pos,
				display: 'block'
			}).appendTo(this);


			if($("#mainmenu>li.inpath.has-dropdown").length===0){
				$('<li id="Indicator"></li>').css({
				left : pos+6,
				display: 'block'
			}).appendTo(this);
				$("#blob").css("display","none");
			}
			blob = $('#blob', nav);
			if($('.has-dropdown.inpath').length !== 0){
				//$('.mainIndhold').addClass('slided-down12');

			}


			$('#mainmenu>li.has-dropdown:not(#blob)', nav).click(function() {
				if(pos===-100){ //Hvis der ikke er noget menupunkt valgt - så....
					$("#blob").css({left:$(this).position().left + ($(this).innerWidth()/2)-8});
					pos = 1; //Herefter sætter vi pos = 1 fordi der nu altid er et punkt valgt, og derfor ikke skal ind i denne sætning mere
				}
				$("#search").hide();
				// mouse over
				$("#Indicator").hide();
				$("#blob").css("display","block");
				$(".hover").removeClass('hover');
				$('.has-dropdown.inpath .dropdown').removeAttr("style");
				if(!blob.is(":visible")){
					blob.css({
						left : $(this).position().left + ($(this).innerWidth()/2)-8,
						display : 'block'
					});

				}
				clearTimeout(reset);
				blob.animate(
				{


					left : $(this).position().left + ($(this).innerWidth()/2)-8
				},
				{
					duration : options.speed,
					easing : options.easing,
					queue : false
				});
				$('.mainIndhold').addClass('slided-down12');



			});

		}); // end each

	};

	// if ( $('#mainmenu ul li').children().length ) {
  // $("#blob").css("display","none");$("#Indicator").css("display","block");
//}





})(jQuery);

