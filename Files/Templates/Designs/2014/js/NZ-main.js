/**
 * Wrapper function for console log
 * usage: log('inside coolFunc',this,arguments);
 * http://paulirish.com/2009/log-a-lightweight-wrapper-for-consolelog/
 */
window.log = function(){
  log.history = log.history || [];   // store logs to an array for reference
  log.history.push(arguments);
  if(this.console){
    console.log( Array.prototype.slice.call(arguments) );
  }
};


/* **********************************************
     Begin _global.js
********************************************** */

var App = App || {};

/**
 * Global object for globally accessible methods and values
 * @return {Object} Collection of public methods and values
 */
App.Global = (function() {
  'use strict';

  var IS_TOUCH = !! ( 'ontouchstart' in window );

  ////////////////
  // Public API //
  ////////////////

  return {
    IS_TOUCH: IS_TOUCH
  };

})();


/* **********************************************
     Begin _livesearch.js
********************************************** */

var App = App || {};

/**
 * [description]
 * @return {[type]} [description]
 */
App.LiveSearch = (function() {
  'use strict';

      // Selectors for DOM elements
  var selectors = {
        form          : '.js-livesearch',
        loader        : '.js-livesearch-loader',
        input         : '.js-input',
        results       : '.js-results',
        noResults     : '.js-noresults',
        error         : '.js-error',
        clickAction   : '.js-itemclick-action'
      },

      // Class names to toggle with js
      classNames = {
        loading : 'loading',
        message : 'msg',
        active  : 'active'
      },

      // Store cached references to DOM elements
      // that will be used over and over again
      dom = {},

      // Minimum characters in search input before firing ajax call
      minInputLength = 4,

      // Webservice for live search
      ajaxUrl,

      // Webservice for item click posts
      ajaxUrlClick,

      // Timeout on keyup before firing ajax call
      keyupTimeout = 350, // ms

      // Current timeout on keyup (for clearing previous keyup timeout)
      currentTimeout,

      // Current ajax request (for cancelling previous request before sending a new)
      currentRequest,

      // Feedback message to user when no results
      noResultsMsg,

      // Feedback to user on ajax error
      errorMsg,

      // Checks if ajax request has results
      hasResults = false;

  /**
   * Initialize function
   * @return {void}
   */
  function initialize() {
    // Set up DOM and cache references
    _setupDOM();

    // Add event listeners
    _addEventListeners();

    // Feedback messages
    noResultsMsg =  $( selectors.noResults, dom.$form ).val();
    errorMsg =      $( selectors.error, dom.$form ).val();
    ajaxUrlClick =  $( selectors.clickAction, dom.$form ).val();
    ajaxUrl =       dom.$form.attr( 'action' );
  }

  /**
   * Set up DOM (create?) elements and cache references for future use
   * @return {void}
   */
  function _setupDOM() {
    dom.$form             = $( selectors.form );

    // Search input
    dom.$input            = $( selectors.input, dom.$form );

    // Loader class
    dom.$loader           = $( selectors.loader );

    // Target element
    dom.$results          = $( selectors.results, dom.$form );

    dom.$close_box        = $( '.js-close-box' );
    dom.$fiber_box_true   = $( '.js-fiber-found' );
    dom.$fiber_box_false  = $( '.js-fiber-not-found' );
    dom.$submit_button    = $( '.js-submit-button' );

  }

  /**
   * Attach event listeners to DOM elements
   */
  function _addEventListeners() {
    dom.$input.on( 'keyup', _onInputKeyup );
    dom.$form.on( 'submit', _onFormSubmit );
    dom.$close_box.on( 'click', _onCloseClick );
  }


  /**
   * [_onCloseClick - Removes active class]
   */
  function _onCloseClick() {
    dom.$fiber_box_true.removeClass( classNames.active );
    dom.$fiber_box_false.removeClass( classNames.active );
  }

  /**
   * [_onResultsKeydown]
   * Enables arrow up and down to navigate the resultlist
   */
  function _onResultsKeydown() {
    var $previtem;
    var $nextitem;

    dom.$results.find('li').focus(function() {
      var $this = $(this);
      $previtem = $this.prev('li');
      $nextitem = $this.next('li');
    });

    dom.$input.keydown(function(e) {
      if (e.keyCode === 40) { /* Focus Down */
        $(this).blur();
        dom.$results.find('li:first').focus();
      }
    });

    dom.$results.keydown(function(e) {
        if (e.keyCode === 40) { /* Focus Down */
          $nextitem.focus();
        }
        else if (e.keyCode === 38) { /* Focus Up */
          $previtem.focus();
        }
        if( e.keyCode === 32 || e.keyCode === 13 ) { /* if enter or space */
          $(this).find('li:focus a').trigger('click');
        }
    });
  }

  /**
   * Event handler for 'keyup' on input
   * Trigger a _getAjax function on Keyup
   */
  function _onInputKeyup( event ) {
    event.preventDefault();
    var query = dom.$input.val();
    // 13, 38, 40, 37, 39 is enter, arrow up, down, left or right.
    if ( query.length >= minInputLength && event.which !== 13 && event.which !== 38 && event.which !== 40 && event.which !== 37 && event.which !== 39 ) {
      dom.$loader.addClass( classNames.loading );
      // window.log('clear timeout');
      clearTimeout(currentTimeout);
      // window.log('set timeout');
      currentTimeout = setTimeout(function () {
        // window.log('on timeout');
        _getAjax( query );
      }, keyupTimeout);
    }
  }

  /**
   * Event handler for 'submit' on form
   * Only possible if !hasResults
   */
  function _onFormSubmit( event ) {
    event.preventDefault();
    var data = dom.$input.val();
    if ( data.length >= minInputLength && !hasResults ) {
      _onPostAjax( { 'queryText' : data } );
    }

  }

  /**
   * Call to webservice for live results
   */
  function _getAjax( query ) {
    // window.log($results);
    if (typeof (currentRequest) === "object") {
      // window.log('abort request');
      currentRequest.abort();
    }
    // window.log('send request', query);
    if( typeof( ajaxUrl ) !== 'undefined' ) {
      currentRequest = $.get( ajaxUrl ,
      { queryText: query },
      function ( data ) {
        // window.log('on success', data);
        if ( data.length === 0 ) {
          var $msg = $('<li/>').addClass( classNames.message ).text( noResultsMsg );
          dom.$results.html( $msg );
          dom.$submit_button.removeClass('hide');
          hasResults = false;
        }
        else {
          // Empty results
          dom.$results.html('');
          dom.$submit_button.addClass('hide');
          $.each(data, function (i, result) {
            var $link = $('<a/>').text( result.label );
            // Append click function if ajax url is defined
            if( typeof( ajaxUrlClick ) !== 'undefined' ) {
              $link.attr( 'data-src', result.value );
              $link.on('click', _onResultItemClick );
            }
            else {
              $link.attr( 'href', result.value );
            }
            var $li = $('<li tabindex="0"/>').append($link);
            dom.$results.append($li);
          });
          hasResults = true;
          _onResultsKeydown();
        }
      })
      .error(function () {
        // window.log('on fail');
        var $msg = $('<li/>').addClass( classNames.message ).text( errorMsg );
        dom.$results.html($msg);
      })
      .always(function () {
        // window.log('on always');
        dom.$loader.removeClass( classNames.loading );
      });
    }
  }

  /**
   * Event handler for 'click' on result items
   */
  function _onResultItemClick( event ) {
    event.preventDefault();
    var $this = $( event.currentTarget );
    var data = $this.attr('data-src');

    $this.addClass( classNames.loading );
    _onPostAjax( event, { 'returnVal' : data } );
  }

  /**
   * _onPostAjax - Ajax post data
   * @param {response} if response is !== "" show fiber box
   */
  function _onPostAjax( event, data ) {
    $.post( ajaxUrlClick,
      data,
      function(response) {
        if( response !== "" ) {
          dom.$fiber_box_true.addClass( classNames.active );
          if( event !== undefined ) {
            $( event.currentTarget ).removeClass( classNames.loading );
          }
        }
        else {
          dom.$fiber_box_false.addClass( classNames.active );
        }
      });
  }

  ////////////////
  // Public API //
  ////////////////

  return {
    initialize: initialize
  };

})();


/* **********************************************
     Begin _productcombinations.js
********************************************** */

var App = App || {};

App.ProductCombinations = (function() {

  var dom = {};

  // For live dom queries
  // selectors = {
  //   product_content: '.js-product-list',
  // };

  function initialize() {
    // Set up DOM and cache references
    _setupDOM();

    // Add event listeners
    _addEventListeners();
  }

  /**
   * Set up DOM (create?) elements and cache references for future use
   * @return {void}
   */
  function _setupDOM() {
    // Product group (Waoo! 1)
    dom.$group              = $( '.js-group' );
    // Product (15/15 Mbit)
    dom.$product            = $( '.js-product' );
    // Additional product (Telefonsvarer)
    dom.$additional_product = $( '.js-additional-product' );
    // Additional group (All aditional products)
    dom.$additional_group   = $( '.js-additional-group' );
  }

  /**
   * Attach event listeners to DOM elements
   */
  function _addEventListeners() {
    dom.$group.on( 'change', _onGroupChange );
    dom.$product.on( 'change', _onProductChange );
    dom.$additional_product.on( 'change', _onAdditionalProductChange );
    dom.$additional_group.on( 'change', _onAdditionalGroupChange );
  }

  /**
   * [_onGroupChange]
   * Select inputs from data-combination
   */
  function _onGroupChange() {
    var $this = $(this);
    var combination = $this.data('combination').toString().split('-');
    dom.$product.prop("checked", false);
    $('input[value='+combination[0]+']').prop("checked", true);
    $('input[value='+combination[1]+']').prop("checked", true);
    $('input[value='+combination[2]+']').prop("checked", true);
  }

  /**
   * [_onProductChange]
   * Loop product values and if the values match group data-combination, select the group.
   */
  function _onProductChange() {
    var combination = "";
    dom.$product.each(function() {
      var $this = $(this);
      if( $this.prop("checked") === true ) {
        combination = combination + '-' + $this.val();
      }
    });
    dom.$group.prop('checked', false);
    $('input[data-combination='+combination.substring(1)+']').prop("checked", true);
  }

  /**
   * [_onAdditionalProductChange]
   * Loop additional_product and set variable to "checked" = true
   */
  function _onAdditionalProductChange() {
    var checked = false;
    dom.$additional_product.each(function() {
      var $this = $(this);
      if( $this.prop("checked") === true ) {
        checked = true;
      }
      else {
        checked = false;
        return false;
      }
    });
    if( checked === true ) {
      dom.$additional_group.prop('checked', true);
    }
    else {
      dom.$additional_group.prop('checked', false);
    }
  }

  /**
   * [_onAdditionalGroupChange]
   * If additional group prop checked = true, set alle additional products to checked = false
   */
  function _onAdditionalGroupChange() {
    var $this = $(this);
    if( $this.prop('checked') === true ) {
      dom.$additional_product.prop('checked', true);
    }
    else {
      dom.$additional_product.prop('checked', false);
    }
  }


  ////////////////
  // Public API //
  ////////////////

  return {
    initialize: initialize
  };

}());


/* **********************************************
     Begin _updatecart.js
********************************************** */

var App = App || {};

App.UpdateCart = (function() {

  var dom = {},

  // For live dom queries
  selectors = {
    ajax_cart: ' .js-ajax-cart',
  };

  function initialize() {
    // Set up DOM and cache references
    _setupDOM();

    // Add event listeners
    _addEventListeners();
  }

  /**
   * Set up DOM (create?) elements and cache references for future use
   * @return {void}
   */
  function _setupDOM() {
    dom.$form_elements  = $( '.js-update-cart' );
    dom.$ajax_container = $( '.js-ajax-container' );
  }

  /**
   * Attach event listeners to DOM elements
   */
  function _addEventListeners() {
    dom.$form_elements.on( 'change', _onFormChange );
  }

  /**
   * [_onFormChange ajax load cart from data-ajaxpath]
   */
  function _onFormChange() {
    var url = $( selectors.ajax_cart ).data('ajaxpath');
    $( selectors.ajax_cart ).addClass('loading');
    dom.$ajax_container.load( url + selectors.ajax_cart, function() {
      $( selectors.ajax_cart ).removeClass('loading');
    });

  }





  ////////////////
  // Public API //
  ////////////////

  return {
    initialize: initialize
  };

}());


/* **********************************************
     Begin _form.js
********************************************** */

var App = App || {};

App.Form = (function() {

  var dom = {};

  function initialize() {
    // Set up DOM and cache references
    _setupDOM();

    // Add event listeners
    _addEventListeners();
  }

  /**
   * Set up DOM (create?) elements and cache references for future use
   * @return {void}
   */
  function _setupDOM() {
    dom.$required               = $( 'form' ).find( '[required]' );
    dom.$select_required        = $( 'form select' ).find( '[required]' );
    dom.$form_submit            = $( '.js-form-submit' );
    dom.$input_password         = $( '.js-password' );
    dom.$input_password_confirm = $( '.js-password-confirm' );
    dom.$form_fields            = $( 'form *' );

    // Add required
    dom.$tricker_button   = $( '.js-tricker-required' );
    dom.$target_element   = $( '.js-target-required' );
  }

  /**
   * Attach event listeners to DOM elements
   */
  function _addEventListeners() {
    dom.$form_submit.on('click', _onSubmitClick);
    dom.$form_fields.on('focusin', _onFormFieldsFocusIn);
    dom.$select_required.on('change', _onFormChange);
    dom.$form_fields.on('keyup', _onFormFieldsKeyUp);
    dom.$form_fields.on('blur', _onFormFieldsKeyUp);
    dom.$tricker_button.on( 'change', _onChangeTrickerButton );
  }

  function _onChangeTrickerButton() {
    if( dom.$tricker_button.prop('checked') === true ) {
      dom.$target_element.attr('required','true');
    }
    else {
      dom.$target_element.removeAttr('required');
    }
  }

  function _onFormFieldsFocusIn() {
    var $this = $(this);
    if( $this.hasClass('required') || $this.prop('required') === true ) {
      $this.removeClass('required');
    }
  }

  function _onFormChange() {
    var $this = $(this);
    if( dom.$form_fields.hasClass('required') || $this.prop('required') === true ) {
      _validation($this);
    }
  }

  function _onFormFieldsKeyUp() {
    var $this = $(this);
    if( $this.hasClass('required') || $this.prop('required') === true ) {
      _validation($this);
    }
  }

  function _validation($this, validation) {
    validation = false;
    var type = $this.attr('type');
    var length = $this.val().length;
    var value = $this.val();
    var mailVal = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

    if( length < 2 ) {
      $this.removeClass('accepted');
      $this.addClass('required');
      validation = false;
    }
    else if( type === 'text' && length === 0 ) {
      $this.removeClass('accepted');
      $this.addClass('required');
      validation = false;
    }
    else if( type === undefined && value === "0" ) {
      $this.removeClass('accepted');
      $this.addClass('required');
      validation = false;
    }
    else if( type === 'checkbox' && $this.prop('checked') === false ) {
      $this.removeClass('accepted');
      $this.addClass('required');
      validation = false;
    }
    else if( type === 'email' && !mailVal.test($this.val()) ) {
      $this.removeClass('accepted');
      $this.addClass('required');
      validation = false;
    }
    else if( type === undefined && value === "0" ) {
      $this.removeClass('accepted');
      $this.addClass('required');
      validation = false;
    }
    else if( $this.hasClass('js-password-confirm') === true && dom.$input_password.val() !== dom.$input_password_confirm.val() ) {
      $this.removeClass('accepted');
      $this.addClass('required');
      validation = false;
    }
    else {
      $this.removeClass('required');
      $this.addClass('accepted');
      validation = true;
    }
    return validation;
  }

  function _onSubmitClick() {
    var validation;
    dom.$required.each(function() {
      var $this = $(this);
      if( $this.is(':visible') === true ) {
        validation = _validation($this, validation);
        if( validation === false ) {
          return false;
        }
      }
    });
    return validation;
  }


  ////////////////
  // Public API //
  ////////////////

  return {
    initialize: initialize
  };

})();


/* **********************************************
     Begin main.js
********************************************** */

// Import external libraries
// codekit-prepend "lib/_some-library.js";

// Import utilities
// @codekit-prepend "utils/_debug-log.js";

// Import partials
// @codekit-prepend "partials/_global.js";
// @codekit-prepend "partials/_livesearch.js";
// @codekit-prepend "partials/_productcombinations.js";
// @codekit-prepend "partials/_updatecart.js";
// @codekit-prepend "partials/_form.js";

var App = App || {};

$(document).ready(function() {

	App.LiveSearch.initialize();
	App.ProductCombinations.initialize();
	App.UpdateCart.initialize();
	App.Form.initialize();

});
