var dates;


function getavailabledates(axid, date){
    var thisaxid = axid;
console.log(thisaxid);
    $.ajax('/CustomHandlers/KS/WebiaBooking.ashx?action=getslots&axid='+thisaxid, {
        type: 'GET',
        datatype: "json",
        data: {
            detailed: true,

        },
        crossDomain : true,
        success: function(data) {
            dates = data;
            console.log(data);
            if(data.length>0){
            createdatepicker(date);
            }else{

                $('#installationstid h5, #installationstid p').remove();
                $('#datepicker').html("<p>Der blev ikke fundet nogen ledige datoer</p>");
                $('#installationstid .knap-f-bestilling').remove();
                $('#installationstid .close-reveal-button').text("OK");
            }
        },
        error: function(data){

            $("#datepicker").html("Der opstod en fejl");
        }


});
}

function getdate(date){
    var result = true;
    //var dateParts = date.split("-");
    //var thisdate = dateParts[2]+"-"dateParts[2]+"-"+dateParts[2];

    $.each(dates, function(i, val){

        var dateParts = val.Date.split("-");
        var thisDate = new Date(dateParts[2],dateParts[1]-1,dateParts[0]);
        if(date.valueOf() == thisDate.valueOf()){
            result = false;
        }

    });
    return result;


}
function createdatepicker(bookingDate){

        if(bookingDate=="01-01-0001"){
            var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();
            bookingDate = dd+"-"+mm+"-"+yyyy;
        }

        var dateParts = bookingDate.split("-");
        var thisBookingDate = new Date(dateParts[2],dateParts[1]-1,dateParts[0]);
        var currentDate = dateParts[2]+"-"+dateParts[1]+"-"+dateParts[0];

$('#datepicker').DatePicker({
    flat: true,
    date: [currentDate],
    current: currentDate,
    format: 'Y-m-d',
    calendars: 1,
    onRender: function(date) {
        return {
            disabled: getdate(date),
            className: date.valueOf() == thisBookingDate.valueOf() ? 'datepickerSpecial' : false
        };
    },
    onChange: function(formated, dates){
        showAvailableSlots(dates);
    },
    starts: 1

});

}

function showAvailableSlots(date){
    var timeslots="";
    var timeslot="";
        var days = ["Mandag", "Tirsdag", "Onsdag", "Torsdag", "Fredag"];
        var month = ["Januar", "Februar", "Marts", "April", "Maj", "Juni", "Juli", "August", "September", "Oktober","November", "December"];
    $.each(dates, function(i, val){
        var dateParts = val.Date.split("-");
        var thisDate = new Date(dateParts[2],dateParts[1]-1,dateParts[0]);
        if(date.valueOf() == thisDate.valueOf()){
            timeslot = "<a data-date='"+val.Date+"' data-interval='"+val.Interval+"' data-baseid='"+val.BaseId+"'>"+days[thisDate.getDay()-1]+" "+thisDate.getDate()+"."+month[thisDate.getMonth()]+" - kl. "+val.Interval+"</a>";
            timeslots += timeslot;
        }

    });
    $("#timepicker").html(timeslots);

}

function book(clicked){

    if($(clicked).hasClass("disabled")){

    }
}

jQuery(document).ready(function($) {
    $("#installationstid .knap-f-bestilling").on("click", function(){
        if(!$(this).hasClass("disabled")){
            $(this).addClass("loading");
            var date = $("#timepicker a.selectedTime").attr("data-date");
            var interval = $("#timepicker a.selectedTime").attr("data-interval");
            var baseid = $("#timepicker a.selectedTime").attr("data-baseid");
            var axid = $("#timepicker a.selectedTime").parents("#installationstid").attr("data-ax");
         $.ajax('CustomHandlers/KS/WebiaBooking.ashx?action=rebook&axid='+axid+'&baseid='+baseid+'&interval='+interval+'&date='+date, {
        type: 'GET',
        datatype: "json",
        crossDomain : true,
        success: function(data) {

            if(data === "true"){
            $(this).removeClass("loading");
            $('#bookok').foundation('reveal', 'open');
             //vi vil opdatere indholdet af data fra serveren.

        }else{
            $('#bookerror').foundation('reveal', 'open');
            $(this).removeClass("loading");
        }

        }


});


        }
    });
});





