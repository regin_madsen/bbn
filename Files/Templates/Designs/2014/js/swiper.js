function loadSwiper(){

  if($(window).width()<1515){


    $('.swiper-slide, .swiper-slide img').attr("style","width:"+$(window).width()+"px !important; min-width:"+$(window).width()+"px !important");

    $('.swiper-slide').addClass("smallslide");
    $('.swiper-container').height($(".swiper-slide img:first-child").height());

    mySwiper = $('.swiper-container').swiper({
        centeredSlides: true,
        slidesPerView:1,
        initialSlide:0,
        updateOnImagesReady:true,
        resizeReInit: false,
        speed: 600,
        loop: true,
        loopAdditionalSlides:0,
        onSwiperCreated: function(){
          var swiperHeight = $("div.swiper-slide:first-child img").innerHeight();
          var TkunderWrapHeight = $(".Tkunder-wrap.hide-for-small").innerHeight();
          var TkundePosTop = (swiperHeight-TkunderWrapHeight)/2;
          if(TkundePosTop < 0){TkundePosTop = 10}
            $(".Tkunder-wrap.hide-for-small").css("top",TkundePosTop);
            $(".cycle-prev, .cycle-next, .Tkunder-wrap").fadeIn('slow');

        }

      });

  }else{
$('.swiper-container').height($(".swiper-slide img:first-child").height());

        mySwiper = $('.swiper-container').swiper({
        centeredSlides: true,
        slidesPerView:'auto',
        initialSlide:0,
        updateOnImagesReady:true,
        resizeReInit: false,
        speed: 600,
        loop: true,
        loopAdditionalSlides:0,
        onSwiperCreated: function(){
            var swiperHeight = $("div.swiper-slide:first-child img").innerHeight();
            var TkunderWrapHeight = $(".Tkunder-wrap").innerHeight();
            var TkundePosTop = (swiperHeight-TkunderWrapHeight)/2;
            $(".Tkunder-wrap.hide-for-small").css("top",TkundePosTop);
            $(".cycle-prev, .cycle-next, .Tkunder-wrap.hide-for-small").fadeIn('slow');
        }

      });

  }
  $('.cycle-prev').on('click', function(e){
    e.preventDefault()
    mySwiper.swipePrev()

  });
  $('.cycle-next').on('click', function(e){
    e.preventDefault()
    mySwiper.swipeNext()
  });
    }



if($(window).width()<1515){

  $(".swiper-container").css("height",$(window).width()*0.376);
 $(".swiper-container, swiper-slide img").css("width",$(window).width());

}
else{$(".swiper-container").css("height","570");}
var mySwiper = "";

$( window ).smartresize(function(){

if($(window).width()<1515){

    $('.swiper-container, .swiper-slide, .swiper-slide img').attr("style","width:"+$(window).width()+"px !important; min-width:"+$(window).width()+"px !important");
    var thisheight = $("div.swiper-slide:first-child img").innerHeight();
    var TkunderWrapHeight = $(".Tkunder-wrap.hide-for-small").innerHeight();
    var TkundePosTop = (thisheight-TkunderWrapHeight)/2;
    if(TkundePosTop < 0){TkundePosTop = 10;}
    $(".Tkunder-wrap.hide-for-small").css("top",TkundePosTop);
    $('.swiper-container').css("height",thisheight);
    $('.swiper-slide').addClass("smallslide");
 if($('.swiper-slide').length>1){
  mySwiper.params.centeredSlides = 'false';
  mySwiper.params.slidesPerView = 1;
  mySwiper.resizeFix(true);
  mySwiper.reInit();
    $(".cycle-prev, .cycle-next, .Tkunder-wrap").fadeIn('slow');
  }else{

          $(".swiper-slide").css('visibility', 'visible').css('opacity','1').addClass('swiper-slide-visible').css("width",$(window).width());
      $(".swiper-slide img").css("width",$(window).width()).css("float","left");
      $(".swiper-slide .overlay").css("width",$(window).width());
  }
}

else{
if($('.swiper-slide').length>1){
  $('.swiper-container').attr("style", "width:100%");
  $('.swiper-slide, .swiper-slide img').attr("style","width:1515px !important; min-width:1515px !important");
  mySwiper.params.centeredSlides = 'true';
  mySwiper.params.slidesPerView = 'auto';
  mySwiper.resizeFix(true);
  mySwiper.reInit();
}else{
     $('.swiper-slide img').width("1515px");
    $(".swiper-slide").css('width','auto').css('visibility', 'visible').css('opacity','1').addClass('swiper-slide-visible');
  }

  $('.swiper-slide').removeClass("smallslide");
  $('.swiper-container').height($(".swiper-slide img:first-child").height());
  $('.swiper-container').css("width","100%");


  var swiperHeight = $("div.swiper-slide:first-child img").innerHeight();
  TkunderWrapHeight = $(".Tkunder-wrap").innerHeight();
  TkundePosTop = (swiperHeight-TkunderWrapHeight)/2;
  $(".Tkunder-wrap.hide-for-small").css("top",TkundePosTop);


}

});


window.onload = function() {
  if($('.swiper-slide').length>1){
  $('.swiper-slide').removeClass("small-12").removeClass("columns");
  loadSwiper();}
  else{
        $(".swiper-container").addClass("noSlides");
      $(".swiper-slide").css('visibility', 'visible').css('opacity','1').addClass('swiper-slide-visible').css("width",$(window).width());
      if($(window).width() > 1515){$(".swiper-slide img").width("1515px");}
        else{
      $(".swiper-slide img").css("width",$(window).width()).css("float","left");
      }
      $(".swiper-slide .overlay").css("width",$(window).width());
      var pos = $("div.swiper-slide img").offset();
      var posright = $("div.swiper-slide img").width();
      var swiperHeight = $("div.swiper-slide img").height();
    var TkunderWrapHeight = $(".Tkunder-wrap.hide-for-small").innerHeight();
    // var TkundePosTop = (swiperHeight-TkunderWrapHeight)/2;
            posright = posright + pos.left - 45;

              $(".noSlides").css("height", swiperHeight);

  }

};

