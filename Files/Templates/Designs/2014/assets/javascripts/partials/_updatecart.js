var App = App || {};

App.UpdateCart = (function() {

  var dom = {},

  // For live dom queries
  selectors = {
    ajax_cart: ' .js-ajax-cart',
  };

  function initialize() {
    // Set up DOM and cache references
    _setupDOM();

    // Add event listeners
    _addEventListeners();
  }

  /**
   * Set up DOM (create?) elements and cache references for future use
   * @return {void}
   */
  function _setupDOM() {
    dom.$form_elements  = $( '.js-update-cart' );
    dom.$ajax_container = $( '.js-ajax-container' );
  }

  /**
   * Attach event listeners to DOM elements
   */
  function _addEventListeners() {
    dom.$form_elements.on( 'change', _onFormChange );
  }

  /**
   * [_onFormChange description]
   * @return {[type]} [description]
   */
  function _onFormChange() {
    var url = $( selectors.ajax_cart ).data('ajaxpath');
    $( selectors.ajax_cart ).addClass('loading');
    dom.$ajax_container.load( url + selectors.ajax_cart, function() {
      $( selectors.ajax_cart ).removeClass('loading');
    });

  }





  ////////////////
  // Public API //
  ////////////////

  return {
    initialize: initialize
  };

}());
