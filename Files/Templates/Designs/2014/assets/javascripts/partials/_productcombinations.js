var App = App || {};

App.ProductCombinations = (function() {

  var dom = {};

  // For live dom queries
  // selectors = {
  //   product_content: '.js-product-list',
  // };

  function initialize() {
    // Set up DOM and cache references
    _setupDOM();

    // Add event listeners
    _addEventListeners();
  }

  /**
   * Set up DOM (create?) elements and cache references for future use
   * @return {void}
   */
  function _setupDOM() {
    // Product group (Waoo! 1)
    dom.$group              = $( '.js-group' );
    // Product (15/15 Mbit)
    dom.$product            = $( '.js-product' );
    // Additional product (Telefonsvarer)
    dom.$additional_product = $( '.js-additional-product' );
    // Additional group (All aditional products)
    dom.$additional_group   = $( '.js-additional-group' );
  }

  /**
   * Attach event listeners to DOM elements
   */
  function _addEventListeners() {
    dom.$group.on( 'change', _onGroupChange );
    dom.$product.on( 'change', _onProductChange );
    dom.$additional_product.on( 'change', _onAdditionalProductChange );
    dom.$additional_group.on( 'change', _onAdditionalGroupChange );
  }

  /**
   * [_onGroupChange description]
   * @return {[type]} [description]
   */
  function _onGroupChange() {
    var $this = $(this);
    var combination = $this.data('combination').split('-');
    $('input[value='+combination[0]+']').prop("checked", true);
    $('input[value='+combination[1]+']').prop("checked", true);
    $('input[value='+combination[2]+']').prop("checked", true);
  }

  /**
   * [_onProductChange description]
   * @return {[type]} [description]
   */
  function _onProductChange() {
    var combination = "";
    dom.$product.each(function() {
      var $this = $(this);
      if( $this.prop("checked") === true ) {
        combination = combination + '-' + $this.val();
      }
    });
    dom.$group.prop('checked', false);
    $('input[data-combination='+combination.substring(1)+']').prop("checked", true);
  }

  function _onAdditionalProductChange() {
    var checked = false;
    dom.$additional_product.each(function() {
      var $this = $(this);
      if( $this.prop("checked") === true ) {
        checked = true;
      }
      else {
        checked = false;
      }
    });
    if( checked === true ) {
      dom.$additional_product.prop('checked', false);
      dom.$additional_group.prop('checked', true);
    }
    else {
      dom.$additional_group.prop('checked', false);
    }
  }

  function _onAdditionalGroupChange() {
    var $this = $(this);
    if( $this.prop('checked') === true ) {
      dom.$additional_product.prop('checked', false);
    }
  }


  ////////////////
  // Public API //
  ////////////////

  return {
    initialize: initialize
  };

}());
