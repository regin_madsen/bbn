var App = App || {};

/**
 * [description]
 * @return {[type]} [description]
 */
App.LiveSearch = (function() {
  'use strict';

      // Selectors for DOM elements
  var selectors = {
        form          : '.js-livesearch',
        loader        : '.js-livesearch-loader',
        input         : '.js-input',
        results       : '.js-results',
        noResults     : '.js-noresults',
        error         : '.js-error',
        clickAction   : '.js-itemclick-action'
      },

      // Class names to toggle with js
      classNames = {
        loading : 'loading',
        message : 'msg',
        active  : 'active'
      },

      // Store cached references to DOM elements
      // that will be used over and over again
      dom = {},

      // Minimum characters in search input before firing ajax call
      minInputLength = 4,

      // Webservice for live search
      ajaxUrl,

      // Webservice for item click posts
      ajaxUrlClick,

      // Timeout on keyup before firing ajax call
      keyupTimeout = 350, // ms

      // Current timeout on keyup (for clearing previous keyup timeout)
      currentTimeout,

      // Current ajax request (for cancelling previous request before sending a new)
      currentRequest,

      // Feedback message to user when no results
      noResultsMsg,

      // Feedback to user on ajax error
      errorMsg,

      // Checks if ajax request has results
      hasResults = false;

  /**
   * Initialize function
   * @return {void}
   */
  function initialize() {
    // Set up DOM and cache references
    _setupDOM();

    // Add event listeners
    _addEventListeners();

    // Feedback messages
    noResultsMsg =  $( selectors.noResults, dom.$form ).val();
    errorMsg =      $( selectors.error, dom.$form ).val();
    ajaxUrlClick =  $( selectors.clickAction, dom.$form ).val();
    ajaxUrl =       dom.$form.attr( 'action' );
  }

  /**
   * Set up DOM (create?) elements and cache references for future use
   * @return {void}
   */
  function _setupDOM() {
    dom.$form             = $( selectors.form );

    // Search input
    dom.$input            = $( selectors.input, dom.$form );

    // Loader class
    dom.$loader           = $( selectors.loader );

    // Target element
    dom.$results          = $( selectors.results, dom.$form );

    dom.$close_box        = $( '.js-close-box' );
    dom.$fiber_box_true   = $( '.js-fiber-found' );
    dom.$fiber_box_false  = $( '.js-fiber-not-found' );
    dom.$submit_button    = $( '.js-submit-button' );
  }

  /**
   * Attach event listeners to DOM elements
   */
  function _addEventListeners() {
    dom.$input.on( 'keyup', _onInputKeyup );
    dom.$form.on( 'submit', _onFormSubmit );
    dom.$close_box.on( 'click', _onCloseClick );
  }

  /**
   * [_onCloseClick description]
   * @return {[type]} [description]
   */
  function _onCloseClick() {
    dom.$fiber_box_true.removeClass( classNames.active );
    dom.$fiber_box_false.removeClass( classNames.active );
  }

  /**
   * [_onResultsKeydown description]
   * @param  {[type]} e [description]
   * @return {[type]}   [description]
   */
  function _onResultsKeydown() {
    var $previtem;
    var $nextitem;

    dom.$results.find('li').focus(function() {
      var $this = $(this);
      $previtem = $this.prev('li');
      $nextitem = $this.next('li');
    });

    dom.$input.keydown(function(e) {
      if (e.keyCode === 40) { /* Focus Down */
        $(this).blur();
        dom.$results.find('li:first').focus();
      }
    });

    dom.$results.keydown(function(e) {
        if (e.keyCode === 40) { /* Focus Down */
          $nextitem.focus();
        }
        else if (e.keyCode === 38) { /* Focus Up */
          $previtem.focus();
        }
        if( e.keyCode === 32 || e.keyCode === 13 ) { /* if enter or space */
          $(this).find('li:focus a').trigger('click');
        }
    });
  }

  /**
   * Event handler for 'keyup' on input
   * @param  {object} event event object
   * @return {void}
   */
  function _onInputKeyup( event ) {
    event.preventDefault();
    var query = dom.$input.val();
    // 13 is enter, 38, 40, 37, 39 is arrow up, down, left or right.
    if ( query.length >= minInputLength && event.which !== 13 && event.which !== 38 && event.which !== 40 && event.which !== 37 && event.which !== 39 ) {
      dom.$loader.addClass( classNames.loading );
      // window.log('clear timeout');
      clearTimeout(currentTimeout);
      // window.log('set timeout');
      currentTimeout = setTimeout(function () {
        // window.log('on timeout');
        _getAjax( query );
      }, keyupTimeout);
    }
  }

  /**
   * Event handler for 'submit' on form
   * @param  {object} event event object
   * @return {void}
   */
  function _onFormSubmit( event ) {
    event.preventDefault();
    var data = dom.$input.val();
    if ( data.length >= minInputLength && !hasResults ) {
      _onPostAjax( { 'queryText' : data } );
    }

  }

  /**
   * Call to webservice for live results
   * @param  {String} query   search query
   * @return {void}
   */
  function _getAjax( query ) {
    // window.log($results);
    if (typeof (currentRequest) === "object") {
      // window.log('abort request');
      currentRequest.abort();
    }
    // window.log('send request', query);
    if( typeof( ajaxUrl ) !== 'undefined' ) {
      currentRequest = $.get( ajaxUrl ,
      { queryText: query },
      function ( data ) {
        // window.log('on success', data);
        if ( data.length === 0 ) {
          var $msg = $('<li/>').addClass( classNames.message ).text( noResultsMsg );
          dom.$results.html( $msg );
          dom.$submit_button.removeClass('hide');
          hasResults = false;
        }
        else {
          // Empty results
          dom.$results.html('');
          dom.$submit_button.addClass('hide');
          $.each(data, function (i, result) {
            var $link = $('<a/>').text( result.label );
            // Append click function if ajax url is defined
            if( typeof( ajaxUrlClick ) !== 'undefined' ) {
              $link.attr( 'data-src', result.value );
              $link.on('click', _onResultItemClick );
            }
            else {
              $link.attr( 'href', result.value );
            }
            var $li = $('<li tabindex="0"/>').append($link);
            dom.$results.append($li);
          });
          hasResults = true;
          _onResultsKeydown();
        }
      })
      .error(function () {
        // window.log('on fail');
        var $msg = $('<li/>').addClass( classNames.message ).text( errorMsg );
        dom.$results.html($msg);
      })
      .always(function () {
        // window.log('on always');
        dom.$loader.removeClass( classNames.loading );
      });
    }
  }

  /**
   * Event handler for 'click' on result items
   * @param  {object} event event object
   * @return {void}
   */
  function _onResultItemClick( event ) {
    event.preventDefault();
    var $this = $( event.currentTarget );
    var data = $this.attr('data-src');

    $this.addClass( classNames.loading );
    _onPostAjax( event, { 'returnVal' : data } );
  }

  function _onPostAjax( event, data ) {
    $.post( ajaxUrlClick,
      data,
      function(response) {
        if( response !== "" ) {
          dom.$fiber_box_true.addClass( classNames.active );
          if( event !== undefined ) {
            $( event.currentTarget ).removeClass( classNames.loading );
          }
        }
        else {
          dom.$fiber_box_false.addClass( classNames.active );
        }
      })
      .fail(function() {
        alert( errorMsg );
      });
  }

  ////////////////
  // Public API //
  ////////////////

  return {
    initialize: initialize
  };

})();
