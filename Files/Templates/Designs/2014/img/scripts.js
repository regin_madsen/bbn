  $(function(){
    $(document).foundation();

    $("img.lazy").lazyload({ effect : "fadeIn"});
  });

  $(".myBox, .myBox2").has("a").addClass('boxeffect');
  $(".boxeffect").click(function(){
        if($(this).find("a").attr("target") == "_blank"){
      window.open($(this).find("a").attr("href"),"_blank");
  }else{
    window.open($(this).find("a").attr("href"),"_top");
  }
      return false;
  });
    $('.active').on('shown.active', function(){
        $(this).parent().find(".fi-plus").removeClass("fi-plus").addClass("fi-minus");
        }).on('hidden.bs.active', function(){
        $(this).parent().find(".fi-minus").removeClass("fi-minus").addClass("fi-plus");
    });
        $('#search-trigger')
        .bind('click', function (e) {
            var search = $('#search');
            $('nav > ul > li > a')
                .trigger('collapse');
            $('.nav-toggle')
                .trigger('collapse');
            if (search.is(':hidden')) {
                $('ul#mainmenu')
                    .find('li.expanded')
                    .removeClass('expanded')
                    .next()
                    .removeClass('hover');
                search.slideDown(200, function () {
                    $('.search-query')
                        .focus();
                });
                $('.mainIndhold')
                    .addClass('slided-down');
            } else {
                search.slideUp(200);
                $('.mainIndhold')
                    .removeClass('slided-down');
            }
            e.preventDefault();
        });
    $('#search')
        .keydown(function (e) {
            if (e.keyCode === 27) {
                $('#search .close')
                    .trigger('click');
            }
        })
    $('#search .close')
        .bind('click', function (e) {
            $('.search-query')
                .val('');
            $('#search')
                .slideUp(200);
            $('.mainIndhold')
                .removeClass('slided-down');

            $('#search-trigger')
                .focus();
            e.preventDefault();
        });
    if ($('body')
        .hasClass('search-results')) {
        $('#search')
            .slideDown();
        $('.mainIndhold')
            .addClass('slided-down');
    }
    $(document).click(function (e) {
    e.stopPropagation();
    var container = $(".has-dropdown");

})
  $('#mainmenu').spasticNav();